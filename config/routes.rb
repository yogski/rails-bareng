Rails.application.routes.draw do
  resources :customers
  resources :gadgets
  get 'test/index'
  get 'test/dipinjam'
  get 'test/tersedia'
  get 'test/kembali'
  get 'test/telat'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
