class CreateGadgets < ActiveRecord::Migration[6.0]
  def change
    create_table :gadgets do |t|
      t.string :name
      t.string :gadget_type
      t.float :rent_price

      t.timestamps
    end
  end
end
