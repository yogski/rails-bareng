class CreateGadgetRent < ActiveRecord::Migration[6.0]
  def change
    create_join_table :gadgets, :customers do |t|
      t.index :gadget_id
      t.index :customer_id
      t.datetime :rent_date
      t.datetime :return_date
      t.datetime :actual_return_date
    end
  end
end
