# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Gadget.create(name: 'AsusL01', gadget_type: 'Laptop', rent_price: 200000, status: 0)
Gadget.create(name: 'CanonDSLR01', gadget_type: 'Camera', rent_price: 500000, status: 0)
Gadget.create(name: 'PS401', gadget_type: 'Game Console', rent_price: 100000, status: 1)
Gadget.create(name: 'NikonDSLR01', gadget_type: 'Camera', rent_price: 500000, status: 1)
Gadget.create(name: 'MacbookPro01', gadget_type: 'Laptop', rent_price: 400000, status: 0)

Customer.create!([
    { name: "Azhari", phone: "087612534129", address: "Jl. Lingkar Luar, Pogung Lor"},
    { name: "Ariqa", phone: "086512437829", address: "Jl. Lingkar Dalam, Kaliurang"},
    { name: "Ihsan", phone: "08652438172", address: "Jl. Lingkar Dalam dan Luar, Pogung Raya"},
    { name: "Ono", phone: "081325637289", address: "Jl. Lingkar Samping, Pogung Lord"},
    { name: "Restu", phone: "08162534256", address: "Jl. Pasar Kembang Regency, Malioboro"},
  ])
