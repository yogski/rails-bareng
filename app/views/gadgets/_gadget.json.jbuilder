json.extract! gadget, :id, :name, :gadget_type, :rent_price, :created_at, :updated_at
json.url gadget_url(gadget, format: :json)
